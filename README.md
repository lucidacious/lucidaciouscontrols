Core engine
=========================

Produces a web application from a json document stored in firebase.  
Apps can be daisy chained by loading a  different json document from firebase.
Apps can be updated on the fly simply by updaing the json document.

This program is an interpreter.  A companion compiler will also be created.
A designer produces the app json document.
 
 
 
## Core Technologies
- [React](https://github.com/facebook/react)
- [Redux](https://github.com/gaearon/redux)
- [React Redux](https://github.com/reactjs/react-redux)
- [Babel 6](https://github.com/babel/babel)
- Sass modules ([sass-loader](https://github.com/jtangelder/sass-loader) [css-loader](https://github.com/webpack/css-loader) [style-loader](https://github.com/webpack/style-loader))
- [redux-logger](https://github.com/fcomb/redux-logger)
- [react-document-meta](https://github.com/kodyl/react-document-meta)
- [karma](https://github.com/karma-runner/karma) 
- [mocha](https://github.com/mochajs/mocha) 

## layout
    actionCreators - Do one of three things.
                        1. dispatch actions to reducers in order to change to a new state
                        2. set up dblisteners which register an action to fire when data changes at a specified path
                        3. update the database at a specified path

    coreComponents - React dumb components.  They do not contain subcomponents.
                 All information necessary to create the component is passed through React properties.
                 Each component is a directory with the code in index.js and local styling in style.scss.
                 All styles should be done through local SASS file.  Globals and functions should be imported.
    constants -  various constants.  An index.js combines them into a single object.
                  actionTypes should contain any ACTION that a reducer resolves.
    coreContainers - React dumb components that are composed of subcompents.  
                    The exception is the App component that connects to state and generates all the other components.
                    They may or may not have local SASS files.
    reducers - These functions create a newstate object from the old state object based on the
                actions dispatched through actions.  THESE MUST BE immutable.
    store - configures the redux store with middleware and contains the initial state.
    test - tests
    utils - utlities and additional logic
            coreUI - read viewTemplates and generates the view out of react components.
            coreUtility - creates an actionCreator from actionCreator templates
                          creates transformations of the context object
                          setter and getter of object trees.

    
## firebase
   Firebase is pub/sub.  The action creators talk to firebase.  One action creator will
   create a listener to a firebase location.  Anytime that locations data changes it
   dispatches an action which a reducer uses to return a new state.
   Data IS NEVER SYNCHRONIZED it is one way.
   Action Creator->Register Firebase Listener with a path and registers an action to be dispatched when data changes
    Action Creator->Firebase Update
    Update event occurs and dispatches the registered action to the reducer.
    Reducer updateds state.
  
   