/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React, { Component } from 'react';

/* component styles */
import { styles } from './styles.scss';

export class Modal extends Component {
    static propTypes = {
        cancel: React.PropTypes.func,
        visible: React.PropTypes.bool.isRequired,
        title:  React.PropTypes.string.isRequired
    };
    constructor(props) {
        super(props);
        this.state = {visible: props.visible}

    }

    render() {
        const {cancel,title,children} = this.props;
        const {visible} = this.state;

        return (
                <div className={`${styles}` } >
                    <div className={`backdrop  ${visible &&"visible"}`}/>

                    <div className={`dialog ${visible &&"visible"}`}>
                        <div className="header" >
                            {this.props.title}
                            <button onClick={()=>{this.setState({visible:false}); console.log(visible); cancel()}}><i className="fa fa-times"/></button>
                        </div>
                        {visible && children}
                    </div>
                </div>

        );
    }
}
