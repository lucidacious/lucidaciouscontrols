/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */

import React from 'react';

/* component styles */
import { styles } from './styles.scss';


export const TextArea =({disabled,error,label,onChange,placeholder,value})=>(


    <div className={`${styles}` }>


            <label>{label}
                <textarea className={`${error && "input-error"}`}
                       disabled={disabled}
                       value={value} placeholder={placeholder}
                       onChange={(e)=>onChange(e.target.value)}/>



            </label>
            {error && <div className= "error" >{error}</div>}



    </div>
);

TextArea.propTypes = {
    disabled: React.PropTypes.bool,
    error: React.PropTypes.string,
    label: React.PropTypes.string,
    onChange: React.PropTypes.func.isRequired,
    placeholder: React.PropTypes.string,
    value: React.PropTypes.string
};
TextArea.defaultProps = {disabled:false};
