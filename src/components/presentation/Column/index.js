/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React from 'react';

export const Column=({children,columns})=>(
    <div className={columns}>
        {children}
    </div>);

Column.propTypes={
    children: React.PropTypes.node,
    columns:React.PropTypes.string.isRequired
}