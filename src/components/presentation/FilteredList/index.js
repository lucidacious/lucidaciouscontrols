/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React, { Component } from 'react';

/* component styles */
import { styles } from './styles.scss';
import {HoverMenu} from 'components/presentation/HoverMenu'

const monthNames = [
    "Jan","Feb","Mar","Apr",
    "May","Jun","Jul","Aug",
    "Sep","Oct","Nov","Dec"
];
 function dateFormat(ts){
    let hourQualifier="AM"
    var theDate = new Date(ts);


    let monthIndex = theDate.getMonth();
    let year = theDate.getFullYear();
    let day = theDate.getDate();
    let hour = theDate.getHours();
    if (hour>12) {
        hour = hour -12;
        hourQualifier="PM"
    }
    let minutes = theDate.getMinutes();
    if (minutes.toString().length==1){
        minutes = '0'+minutes.toString()
    }
    return `${monthNames[monthIndex]} ${day}, ${year} ${hour}:${minutes} ${hourQualifier}`
}




export class FilteredList extends Component {
    static propTypes = {
        displayFields: React.PropTypes.arrayOf(React.PropTypes.shape({
            display: React.PropTypes.string,
            field: React.PropTypes.string,
            format: React.PropTypes.string
        })),
        itemMenu:React.PropTypes.arrayOf(React.PropTypes.shape({
            action: React.PropTypes.func,
            passback: React.PropTypes.string,
            text: React.PropTypes.string,
            icon: React.PropTypes.string
        })),
        list: React.PropTypes.arrayOf(React.PropTypes.object),
        onItemClick:React.PropTypes.func
    };

    constructor(props) {
        super(props);
        this.state = {
            filter: "",
            sortBy: "",
            descending: false
        }
    }



    static formatted(item, heading) {

        if (heading.format === "date") {
            return dateFormat(item[heading.field]);
        }
        else {
            return item[heading.field]
        }
    }

    buildHeaderDiv(fieldsToDisplay, colClass) {
        const sortKey = this.state.sortBy;
        const isDescending = this.state.descending;
        const icon = (isDescending) ? <i className="fa fa-caret-down"/> : <i className="fa fa-caret-up"/>;

        return fieldsToDisplay.map(item=> {
            return <div key={`${item.field} header`} className={`${colClass} column-head`}
                        onClick={()=>{  this.setState({sortBy:item.field,descending:!isDescending})}}>{item.display}&nbsp;
                {(item.field === sortKey) && icon}
            </div>
        })

    }



    render() {
        const {displayFields,list,itemMenu,onItemClick}=this.props;

        const numColumns = (displayFields.length);
        const colWidth = Math.floor(10 / numColumns);
        const colClass = `col-xs-${colWidth} col-sm-${colWidth} col-md-${colWidth} col-lg-${colWidth}`;
        const colClassHeader =  `col-xs-${12-(colWidth*numColumns)} col-sm-${12-(colWidth*numColumns)} col-md-${12-(colWidth*numColumns)} col-lg-${12-(colWidth*numColumns)}`;
        const header = this.buildHeaderDiv(displayFields, colClass);

        

        const itemArr = list.filter(item =>
            (this.state.filter === '' || item[displayFields[0].field].toLowerCase().indexOf(this.state.filter.toLowerCase()) > -1))
            .sort((a, b)=> {
                let aVal = a[this.state.sortBy];
                let bVal = b[this.state.sortBy];
                if (typeof aVal === "string") {
                    aVal = aVal.toLowerCase()
                    bVal = bVal.toLowerCase()
                }
                let direction = (this.state.descending) ? -1 : 1
                if (aVal > bVal) {
                    return direction;
                }
                else if (aVal < bVal) {
                    return -1 * direction;
                }
                else {
                    return 0
                }

            });
        const items = itemArr.map(item=> {
            return (<div key={`${item.key} row`} className="row item"
                         onClick={()=>{onItemClick(item.key)
        }
        }>

                {displayFields.map(p=> {
                    return <div key={`${item.key} ${item[p.field]} col`}
                                className={`${colClass} col`}>{FilteredList.formatted(item, p)}</div>
                })}
                <div className={`${colClassHeader}`}>
                    <div className="menu-wrapper">
                      <HoverMenu pen="default" icon="fa fa-bars" options={itemMenu}/>
                    </div>
                </div>

            </div>)
        })


        return (


            <div className={`${styles}` }>
                <div className="container-fluid">
                    <div className="row bar">
                        {header}
                        <span className={`search ${colClassHeader}`}>
                             <input type="text" placeholder="Search" ref="toolbarFilter"
                                    onChange={(e)=>{this.setState({filter:e.target.value})}} value={this.state.filter}/>
                             <span className="fa fa-search"></span>
                        </span>
                    </div>
                    <div className="item-wrapper">
                    {items}
                    </div>
                </div>
            </div>

        );
    }
}

