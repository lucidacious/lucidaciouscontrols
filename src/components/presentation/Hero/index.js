/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React, { Component } from 'react';
import LoadingOrderAnimation from 'react-loading-order-with-animation';


/* utils */


/* component styles */
import { styles } from './styles.scss';

function isMobileAndTablet() {
  return (window.innerWidth <= 800 && window.innerHeight <= 600)

}
function setParallax(elem, speed = 30) {
    const top = (window.pageYOffset - elem.offsetTop) / speed;

    isMobileAndTablet
        ? elem.style.backgroundPosition = `0px ${ top }px`
        : null;
}


export class Hero extends Component {
  static propTypes = {
    title: React.PropTypes.string,
    description: React.PropTypes.string,
    imgUrl: React.PropTypes.string.isRequired
  };
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    setParallax(this.refs.parallax, 5);
  };

  render() {
    const {title,description,imgUrl} = this.props;
    return (
      <div className={`${styles} container-fluid`} style={{backgroundSize: "cover",backgroundImage: `url(${imgUrl})`}} ref="parallax">


  <div className="hero text-center" >
            <LoadingOrderAnimation animation="fade-in"
                                   move="from-bottom-to-top"
                                   distance={300}
                                   speed={700}
                                   wait={700}
                >
              <h1 className="title">
                {title}
              </h1>
            </LoadingOrderAnimation>
            <LoadingOrderAnimation animation="fade-in"
                                   move="from-bottom-to-top"
                                   distance={60}
                                   speed={700}
                                   wait={900}
                >
              <p>
                <i>{description}</i>
              </p>
            </LoadingOrderAnimation>
          </div>


      </div>
    );
  }
}
Hero.defaultProps ={title:'',description:''};