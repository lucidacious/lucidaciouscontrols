/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React, { Component } from 'react';
/* component styles */
import { styles } from './styles.scss';


export class Select extends Component {
    static propTypes = {
        disabled: React.PropTypes.bool,
        label: React.PropTypes.string,
        onChange: React.PropTypes.func.isRequired,
        options: React.PropTypes.arrayOf(React.PropTypes.shape({
            value: React.PropTypes.string,
            icon: React.PropTypes.string
        })),
        tabIndex: React.PropTypes.string,
        value: React.PropTypes.string
    };

    constructor(props) {
        super(props);
        this.state = {active: false, value:props.value}
    }

    render() {

        const {disabled,label,onChange,options,tabIndex} = this.props;

        const {active,value} = this.state;
        return (
            <div className={`${styles}` }>
                {active &&<div className="backdrop" onClick={()=>{this.setState({active:false})}}/>}
                <div className="label">{label}</div>
                <div className={`wrapper-dropdown ${disabled && "disabled"} ${active && "active"}`} tabIndex={tabIndex}
                    onClick={()=>{this.setState({active:!active})}}
                    >{value}
                    <ul className="dropdown">
                        {options.map(option=> {
                            return (
                                <li key={`select${option.value}`}>
                                    <a href="#" onClick={(e)=>{
                                    e.stopPropagation();
                                    if (option.value !== value){
                                        this.setState({active:false,value:option.value});
                                        onChange(option.value)
                                        }
                                       else{
                                          this.setState({active:false});
                                        }
                                    }}>
                                        {option.icon && <i className={option.icon}></i>}
                                        {option.value}
                                    </a>
                                </li>)
                        })}

                    </ul>
                </div>

            </div>
        );
    }
}
Select.defaultProps = {disabled:false,tabIndex:"1"};



