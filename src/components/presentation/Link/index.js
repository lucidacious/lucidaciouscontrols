/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React from 'react';
import { styles } from './styles.scss';


export const Link=({href,pen})=>(
    <span className={`${styles}` }>
                <a className={pen} href={href}>
                    {this.props.text}
                </a>
            </span>);

Link.propTypes={

    href:React.PropTypes.string.isRequired,
    pen: React.PropTypes.oneOf(['normal','inverse','warning'])
};


