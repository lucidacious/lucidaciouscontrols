/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React from 'react';



/* component styles */
import { styles } from './styles.scss';


export const Card =({hasFrame,backgroundImage,padding,visible,minHeight,children})=>(

            <div className={`${styles}` } >
                {visible && <div className={`outer padding-${padding} ${hasFrame && "frame"}`}
                     style={{backgroundImage:`url(${backgroundImage})`,backgroundSize: "cover",minHeight:minHeight}}>
                    <div className="inner" >
                        {children}
                    </div>
                </div> }
            </div>
        );

Card.propTypes = {
    hasFrame: React.PropTypes.bool,
    padding: React.PropTypes.oneOf(['none', 'normal', 'extra']),
    visible: React.PropTypes.bool,
    minHeight: React.PropTypes.string,
    backgroundImage: React.PropTypes.string
};
Card.defaultProps = {hasFrame: false, padding: 'normal', visible: true};

