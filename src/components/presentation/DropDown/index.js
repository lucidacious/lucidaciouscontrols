/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React, { Component } from 'react';
/* component styles */
import { styles } from './styles.scss';


export class DropDown extends Component {
    static propTypes = {
        disabled: React.PropTypes.bool,
        label: React.PropTypes.string,
        options: React.PropTypes.arrayOf(React.PropTypes.shape({
            action: React.PropTypes.func,
            passback: React.PropTypes.string,
            text: React.PropTypes.string,
            icon: React.PropTypes.string
        })),
        tabIndex: React.PropTypes.string

    };

    constructor(props) {
        super(props);
        this.state = {active: false}
    }

    render() {

        const {disabled,label,options,tabIndex} = this.props;

        const {active} = this.state;
        return (
            <div className={`${styles}` }>
                {active &&<div className="backdrop" onClick={()=>{this.setState({active:false})}}/>}
                <div className={`wrapper-dropdown ${disabled && "disabled"} ${active && "active"}`} tabIndex={tabIndex}
                    onClick={()=>{this.setState({active:!active})}}>{label}
                    <ul className="dropdown">
                        {options.map(option=> {
                            return (
                                <li key={`li${option.text}`}>
                                    <a href="#" onClick={(e)=>{
                                    e.stopPropagation();
                                    this.setState({active:false});
                                    option.action(option.passback)
                                    }}>
                                        {option.icon && <i className={option.icon}></i>}
                                        {option.text}
                                    </a>
                                </li>)
                        })}

                    </ul>
                </div>

            </div>
        );
    }
}
DropDown.defaultProps = {disabled:false,tabIndex:"1"};



