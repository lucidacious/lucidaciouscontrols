/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React from 'react';
/* component styles */
import { styles } from './styles.scss';



export const Button=({click,icon,text,highlight,passback,disabled})=>(
    <div className={`${styles}`}>
        <button disabled={disabled} className={`${highlight} ${disabled &&'disabled'}`}
                onClick={()=>{click(passback)}}>

            {(icon) && <i alt={text} className={`icon`}>&nbsp;</i> }
            <span className="text">{text}</span>
        </button>
    </div>
);


Button.propTypes = {
    click: React.PropTypes.func.isRequired,
    icon: React.PropTypes.string,
    text:  React.PropTypes.string.isRequired,
    highlight: React.PropTypes.oneOf(['primary','secondary','inverse','inverse-secondary','warning','hover','hover-inverse','hover-warning']),
    passback: React.PropTypes.any,
    disabled: React.PropTypes.bool
};
Button.defaultProps = { highlight: 'primary',disabled:false };

