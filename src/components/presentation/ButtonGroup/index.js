/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React,{Component} from 'react';

/* component styles */
import { styles } from './styles.scss';



export class ButtonGroup extends Component {
    static propTypes = {
        change: React.PropTypes.func.isRequired,
        values: React.PropTypes.string.isRequired,
        buttons: React.PropTypes.string.isRequired,
        id: React.PropTypes.string.isRequired,
        label: React.PropTypes.string,
        enabled: React.PropTypes.bool
    };
    constructor(props) {
        super(props);


    }
    onClick(value){


}
    render() {

        const {enabled,values,buttons,id,change,label} = this.props;
        const valueArray =values.split(",");
        const buttonArray =buttons.split(",");
        const btnGrp = (buttons && buttons.length>0 ) && buttonArray.map(button=>{
                return(
                <button className={`button ${valueArray.includes(button) && 'selected'} ` }
                        key={`${id+button}`} id={`${id+button}`}
                        onClick={()=>{change(button)}}>
                    {button}
                </button>)
            });

        return (
            <div className={`${styles}`}>
                {enabled && buttons.length>0 && <label htmlFor={id}>{label}</label>}
                {enabled && buttons.length>0 &&<span className="btnGroup" id={id}>{btnGrp}</span>}
             </div>
        );
    }
}
ButtonGroup.defaultProps ={enabled:true,buttonStyle:"outline"};