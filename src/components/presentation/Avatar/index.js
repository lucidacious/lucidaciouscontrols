/** Copyright (c) 2016 LUCIDACIOUS LLC
  SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React from 'react'
import md5 from 'MD5';
import {Button} from 'components/presentation/Button'
/* component styles */
import { styles } from './styles.scss';

export const Avatar = ({email})=>(<div className={`${styles}`}>
    <img src={`https://www.gravatar.com/avatar/${md5(email)}?d=mm&s=48`}/>
</div>);

Avatar.propTypes = {

    email: React.PropTypes.string.isRequired

};

