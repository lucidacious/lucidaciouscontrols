/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React from 'react';

/* component styles */
import { styles } from './styles.scss';


export const Input =({disabled,error,label,onChange,placeholder,type,value})=>(


    <div className={`${styles}` }>


            <label>{label}
                <input className={`${error && "input-error"}`} type={type}
                       disabled={disabled}
                       value={value} placeholder={placeholder}
                       onChange={(e)=>onChange(e.target.value)}/>

            </label>
            {error && <div className= "error" >{error}</div>}



    </div>
);

Input.propTypes = {
    disabled: React.PropTypes.bool,
    error: React.PropTypes.string,
    label: React.PropTypes.string,
    onChange: React.PropTypes.func.isRequired,
    placeholder: React.PropTypes.string,
    type: React.PropTypes.oneOf(['date','email','file','month','number','text','url']),
    value: React.PropTypes.string
};
Input.defaultProps = {disabled:false,type:"text"};
