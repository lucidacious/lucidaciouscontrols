/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React, { Component } from 'react';

/* component styles */
import { styles } from './styles.scss';

export class TopNav extends Component {
    static propTypes = {
        navStyle: React.PropTypes.oneOf(["fixed", "relative"])
    }
    componentDidMount() {
        if (this.props.navStyle==='fixed'){
            window.addEventListener('scroll', this.handleScroll);
        }
    }

    componentWillUnmount() {
        if (this.props.navStyle==='fixed'){
            window.removeEventListener('scroll', this.handleScroll);
        }
    }

    handleScroll = (event) => {
        if (this.props.navStyle==='fixed'){
            const elem = this.refs.topnav;
        //    elem.className = (event.srcElement.body.scrollTop > 0) ? "fixed-scroll" : "fixed"
        }
    };

    render() {
        return (
            <div className={`${styles}`} style={{margin:0,position:this.props.navStyle}}>
                <div className={`${this.props.navStyle} `} ref="topnav">
                    <div className="container">
                        <div className="row logo">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
TopNav.defaultProps ={navStyle:"fixed"};
