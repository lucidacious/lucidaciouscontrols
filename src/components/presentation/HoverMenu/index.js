/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React, { Component } from 'react';
/* component styles */
import { styles } from './styles.scss';


export class HoverMenu extends Component {
    static propTypes = {
        icon: React.PropTypes.string,
        label: React.PropTypes.string,
        options: React.PropTypes.arrayOf(React.PropTypes.shape({
            action: React.PropTypes.func,
            passback: React.PropTypes.string,
            text: React.PropTypes.string,
            icon: React.PropTypes.string
        })),
        pen:React.PropTypes.oneOf(['default','primary','inverse'])

    };

    constructor(props) {
        super(props);
        this.state = {active: false}
    }

    render() {

        const {options,label,pen,icon} = this.props;
        const {active} = this.state;
        return (
            <div className={`${styles}` }>
                {active &&<div className="backdrop" onMouseOver={()=>{this.setState({active:false})}}/>}
                <div className={`wrapper-dropdown ${active && "active"}`}
                     onMouseOver={()=>{this.setState({active:true})}}>
                    <span className={`menu ${pen} `}><i className={icon}/>{label}</span>
                    <ul className="dropdown">
                        {options.map(option=> {
                            return (
                                <li key={`hover${option.text}`}>
                                    <a href="#" onClick={(e)=>{
                                    e.stopPropagation();
                                    this.setState({active:false});
                                    option.action(option.passback)
                                    }}>
                                        {option.icon && <i className={option.icon}></i>}
                                        {option.text}
                                    </a>
                                </li>)
                        })}

                    </ul>
                </div>

            </div>
        );
    }
}
HoverMenu.defaultProps = {pen:"default",icon:""};



