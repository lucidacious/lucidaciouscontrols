/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React from 'react';

export const Row=({children})=>(
    <div className="row">
                {children}
    </div>)
Row.propTypes={
    children: React.PropTypes.node
}
