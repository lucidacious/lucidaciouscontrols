/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */
import React from 'react';

export const Container=({children,isFluid})=>(
    <section className={(isFluid)?"container-fluid":"container"}>
        {children}
    </section>);
Container.propTypes={
    children: React.PropTypes.node,
    isFluid: React.PropTypes.bool
};
Container.defaultProps ={isFluid:false};

