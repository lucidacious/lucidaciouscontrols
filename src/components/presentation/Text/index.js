/** Copyright (c) 2016 LUCIDACIOUS LLC
 SUBJECT TO TERMS OF THE LICENSE AGREEMENT INCLUDED WITH THIS SOFTWARE.
 */


import React from 'react';
/* component styles */
import { styles } from './styles.scss';



export const Text=({text,tag,align,font,pen})=>(
    <div className={`${styles}`}>
        {
            (()=> {

                switch (tag) {
                    case 'p':
                        return <p className={`${font} ${pen}`} style={{textAlign:align}}>{text}</p>
                    case 'h1':
                        return <h1 className={`${font} ${pen}`} style={{textAlign:align}}>{text}</h1>
                    case 'h2':
                        return <h2 className={`${font} ${pen}`} style={{textAlign:align}}>{text}</h2>
                    case 'h3':
                        return <h3 className={`${font} ${pen}`} style={{textAlign:align}}>{text}</h3>
                    case 'h4':
                        return <h4 className={`${font} ${pen}`} style={{textAlign:align}}>{text}</h4>
                    case 'em':
                        return <em className={`${font} ${pen}`} style={{textAlign:align}}>{text}</em>
                    case 'code':
                        return <code className={`${font} ${pen}`} style={{textAlign:align}}>{text}</code>
                    case 'strong':
                        return <strong className={`${font} ${pen}`} style={{textAlign:align}}>{text}</strong>
                    case 'cite':
                        return <cite className={`${font} ${pen}`} style={{textAlign:align}}>{text}</cite>
                    default:
                        return <span className={`${font} ${pen}`} style={{textAlign:align}}>{text}</span>
                }
            })()


        }
    </div>
);
Text.propTypes = {
    align: React.PropTypes.oneOf(["left","center","right"]),
    tag: React.PropTypes.string,
    text:  React.PropTypes.string.isRequired,
    font: React.PropTypes.oneOf(['primary','brand','control']),
    pen: React.PropTypes.oneOf(['normal','inverse','warning'])
};



Text.defaultProps ={tag:'p',font:"primary",pen:"normal",align:"left"};

