import React from 'react';
import ReactDOM from 'react-dom';
import {Container} from 'components/presentation/Container'
import {Row} from 'components/presentation/Row'
import {Column} from 'components/presentation/Column'
import {Button} from 'components/presentation/Button'
import {Avatar} from 'components/presentation/Avatar'
import {Text} from 'components/presentation/Text'

import {Card} from 'components/presentation/Card'
import {Input} from 'components/presentation/Input'
import {TextArea} from 'components/presentation/TextArea'
import {DropDown} from 'components/presentation/DropDown'
import {HoverMenu} from 'components/presentation/HoverMenu'
import {Select} from 'components/presentation/Select'
import {Modal} from 'components/presentation/Modal'
import {Hero} from 'components/presentation/Hero'
import {ButtonGroup} from 'components/presentation/ButtonGroup'
import {FilteredList} from 'components/presentation/FilteredList'
import 'components/presentation/styles/grid12.scss'
var modalVisible =false;
ReactDOM.render(<div>

    <Container>
        <Row>
            <FilteredList list={[{key:"one",name:"file 1",modified: new Date()},
                                {key:"two",name:"file 2",modified: new Date()},
                                {key:"three",name:"file 3",modified: new Date()}]}
                        displayFields={[{display:"Name",field:"name"},{display:"Date modified",field:"modified",format:"date"}]}
                          itemMenu={[{passback:"1",text:"One",action:(pb)=>{console.log("1.",pb)}},{passback:"2",action:(pb)=>{console.log("oo.",pb)},text:"Two"},{passback:"3",action:(pb)=>{console.log("three.",pb)},text:"Three"}]}
                      onItemClick={(value)=>{console.log("filterlist",value)}}

                />
        </Row>
        <Row>
            <Column columns="col-md-3">
                <DropDown label="Click Me" onClick={(value)=>{console.log("picked",value)}}
                          options={[{passback:"1",text:"One",icon:"fa fa-times",action:(pb)=>{console.log("1.",pb)}},{passback:"2",action:(pb)=>{console.log("oo.",pb)},text:"Two"},{passback:"3",action:(pb)=>{console.log("three.",pb)},text:"Three"}]}/>
            </Column>
            <Column columns="col-md-3">
                <Select value="Two" onChange={(value)=>{console.log("Selected",value)}} expandOn="hover" label="Label"
                        options={[{value:"One"},{value:"Two"},{value:"Three"}]}/>
            </Column>

            <Column columns="col-md-3">
                <HoverMenu icon="fa fa-times" onClick={(value)=>{console.log("picked",value)}} pen="default" label="click"
                           options={[{passback:"1",text:"One",action:(pb)=>{console.log("1.",pb)}},{passback:"2",action:(pb)=>{console.log("oo.",pb)},text:"Two"},{passback:"3",action:(pb)=>{console.log("three.",pb)},text:"Three"}]}/>
            </Column>

        </Row>
        <Row>
            <Column columns="col-md-3">
                <Input type="text" onChange={()=>{}} label="Label" placeholder="placeholder" value="" error=""/>
                <TextArea onChange={()=>{}} label="Label" placeholder="placeholder" value="" error=""/>
            </Column>
            <Text align="center" text="Hello World" tag="h1" font="brand"/>
            <Text align="right" text="Hello World" tag="h4" font="primary" pen="warning"/>
            <Button click={()=>{ console.log(modalVisible); modalVisible=true}} text="primary" highlight="primary" passback="Clay"/>
            <Button click={(moore)=>{console.log(moore)}} text="secondary" highlight="secondary" passback="Clay"/>

            <Button click={(moore)=>{console.log(moore)}} text="warning" highlight="warning" passback="Clay"/>

            <div style={{backgroundColor:"darkblue"}}>
                <Button click={(moore)=>{console.log(moore)}} text="inverse" highlight="inverse" passback="Clay"/>
                <Button click={(moore)=>{console.log(moore)}} text="inverse" highlight="inverse-secondary"
                        passback="Clay"/>
                <Avatar email="clamors@gmail.com" name="Clay Moore" pen="inverse" click={()=>{console.log("kevin")}}/>
            </div>
        </Row>
        <Row>
            <Column columns="col-md-12">
            <ButtonGroup change={(button)=>{console.log("change",button)}} values="one" buttons="one,two,three" id="bigbutton" label="Label"/>
            </Column>
        </Row>
        booo
        <Row>
            <Column columns="col-md-9">
                <Card hasFrame={true}  backgroundImage="https://upload.wikimedia.org/wikipedia/commons/0/07/Flickr_-_Oregon_Department_of_Fish_%26_Wildlife_-_2313_elk_cows_swart_odfw.jpg">
                    <Avatar email="clamors@gmail.com" name="Clay Moore" click={()=>{console.log("bacon")}}/>
                </Card>
            </Column>

            <Column columns="col-md-3">
                <Card hasFrame={true} minHeight="250px">
                    <Avatar email="clamors@gmail.com" name="Clay Moore" click={()=>{console.log("bacon")}}/>
                </Card>
            </Column>
        </Row>


    </Container></div>
    ,
    document.getElementById('root')
)